package tb.antlr.interpreter;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;

import tb.antlr.symbolTable.LocalSymbols;

public class MyTreeParser extends TreeParser {
	protected LocalSymbols vars = new LocalSymbols();

    public MyTreeParser(TreeNodeStream input) {
        super(input);
    }

    public MyTreeParser(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }

    protected void drukuj(String text) {
        System.out.println(text.replace('\r', ' ').replace('\n', ' '));
    }

	protected Integer getInt(String text) {
		return Integer.parseInt(text);
	}
	
	protected void printuj(String text)
	{
		drukuj(text);
	}
	
	protected Integer add(Integer a, Integer b) {
		return a + b;
	}
	
	protected Integer sub(Integer a, Integer b) {
		return a - b;
	}
	
	protected Integer mul(Integer a, Integer b) {
		return a * b;
	}

    protected int div(int a, int b) throws RuntimeException{
    	if(b == 0) {
    		throw new RuntimeException("Dzielenie przez 0!");
    	}
    	return a / b;
    }

    protected void setVar(String name, Integer value) {
		vars.setSymbol(name, value);
	}


	protected Integer getVar(String name) {
		return vars.getSymbol(name);
	}


	protected void newVar(String name) {
		vars.newSymbol(name);
	}
}
