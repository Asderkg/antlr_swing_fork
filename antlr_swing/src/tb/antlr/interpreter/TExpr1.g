tree grammar TExpr1;

options {
	tokenVocab=Expr;

	ASTLabelType=CommonTree;
  superClass=MyTreeParser;
}

@header {
package tb.antlr.interpreter;
}

prog    : (expr)* ;

expr returns [Integer out]
        : ^(PLUS  e1=expr e2=expr) {$out = add($e1.out, $e2.out);}
        | ^(MINUS e1=expr e2=expr) {$out = sub($e1.out, $e2.out);}
        | ^(MUL   e1=expr e2=expr) {$out = mul($e1.out, $e2.out);}
        | ^(DIV   e1=expr e2=expr) {$out = div($e1.out, $e2.out);}
        | ^(PRINT e1=expr)         {drukuj($e1.out.toString());}
        | ^(VAR name=ID)           {newVar($name.text);}
        | (name=ID)                {$out = getVar($name.text);}
        | ^(PODST name=ID e1=expr) {setVar($name.text, $e1.out);}
        | INT                      {$out = getInt($INT.text);}
        | OB                       {vars.enterScope();}
        | CB                       {vars.leaveScope();}
        ;
